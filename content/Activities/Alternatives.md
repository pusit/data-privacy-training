# Alternatives
--- meta
title: Alternatives
uuid: f87ee79d-03ba-4714-b039-dc4346432a8c
lan: en
source: Tactical Tech
item: Activity
tags:
  - Choosing tools
  - Mobile
  - Secure messaging
  - Browser
duration: 30
description: How are commercial and non-commercial tools and services different? Why does it matter? This activity offers a simple framework for making informed choices, and introduces some alternative options.
---

## Meta information

### Description
How are commercial and non-commercial tools and services different? Why does it matter? This activity offers a simple framework for making informed choices, and introduces some alternative options.

### Duration
35 minutes.


### Ideal Number of Participants
Minimum of 4.


### Learning Objectives
##### Knowledge
- Gain a framework to evaluate tools and services.
- Increase knowledge on open source, and ownership of tools and services.

##### Skills
- Be able to evaluate some key tools.

##### Attitude
- As a user you have to choose what information you want to protect. Do you want to manage your identity, social network, content or location? Protecting one might expose the other.

### References
- [Alternatives](https://myshadow.org/increase-your-privacy#alternatives) (MyShadow/Tactical Tech)

### Materials and Equipment Needed
- @[material](6ebfc3d0-3f8a-41b9-800b-5e802b985fd8)
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)


## Activity: "Alternatives"
--- meta
uuid: bc9d5aee-0106-41dd-9164-7f2157e16260
---

#### Preparation
1. Print an "Alternatives grid" for each participant - make sure it's blank!
2. Download a grid that's been filled in already with specific tools (some can be downloaded from the [Materials](https://myshadow.org/materials) page at MyShadow.org), or fill in a grid yourself, depending on what tools and services you're focusing on. (eg. search engines or messaging apps). You can either print this out for participants, or project it on the wall.

#### Brainstorm apps and tools (5 min)
1. Briefly introduce the activity, and give each participant a blank Alternatives grid.
2. Focusing on a specific type of service (search engines, messaging apps, etc), put participants into pairs and ask them to fill in the first column with the names of some services/apps they can think of (e.g. for messaging apps: Whatsapp, Snapchat, Signal)

#### Go through the Evaluation Framework (20 min)
1. Using one services/app (eg Whatsapp) as an example, walk the group through each category step by step, explaining concepts and answering questions as they arise.
2. Get participants back into pairs and give them time to fill in the rest of the grid.
3. Give them a filled-in grid to compare their own against, and answer any questions.

#### Discuss: How do you decide which tool best suits your own needs? (10 min)
Lead a discussion that covers the following:
1. When choosing a tool, it's important to think about what data you want to "protect". It can be useful to think about this within four broad categories: identity, social networks, content, and location.
2. A tool might, for example, protect your content with encryption, but might also require access to specific information like your phone number, making it impossible to use pseudonymously or anonymously. If pseudonymity/anonymity is what you need, then a different tool might suit your needs better.


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
