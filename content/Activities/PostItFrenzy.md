# Post-it Frenzy
--- meta
title: Post-it Frenzy
uuid: a9c63bca-e39e-4af3-88bb-5dd7cc670bcb
lan: en
source:
item: Activity
tags:
  - Mobile
  - Hands-on
  - Social media
  - Secure messaging
  - Browser
  - Choosing tools
  - Collecting ideas
duration: 20
description: Find out what others in the room think about a specific issue, and then pull out common themes and ideas.
---

## Meta information

### Description
Find out what others in the room think about a specific issue, and then pull out common themes and ideas.

### Duration
20 minutes.

### Ideal Number of Participants
Minimum of 4 participants.

### Learning Objectives
##### Knowledge
- Gain insight into what others are thinking, and expand understanding on a specific topic.

### Materials and Equipment Needed
- @[material](c0358b51-fe16-47ae-9686-927ec39d18f6)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)

## Activity: "Post-it Frenzy"
--- meta
uuid: 5d8316f6-64eb-4a57-9f11-05bfedfac45f
---

#### Preparation
1. Prepare a statement, topic or question for the group to unpack or answer.
2. Make sure there is a space where all the post-its can be placed - this could be a board or wall, or a large piece of paper stuck onto a board or wall.
- Write the statement, topic or question at the top.


#### Brainstorm (10 min)
Give participants a stack of post-its and give them 10-15 minutes to write down as many terms, phrases, ideas that relate to the topic in question, and to stick these onto the board or wall.

#### Organise (5 min)
Ask participants to group the post-its into 'clusters' of similar themes and ideas.

#### Feedback (5 min)
Bring the group together and ask one participant to give an overview of the different clusters.

-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
